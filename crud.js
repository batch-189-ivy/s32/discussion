let http = require('http')

let users = [
	{
		"name": 'Jose Marie Viceral',
		"email": 'jmarie@email.com'
	},
	{
		"name": 'Bayani Agbayani',
		"email": 'bayani@email.com'
	}
]

let port = 4000

let server = http.createServer(function(request, response) {
	if(request.url == '/users' && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'application/json'})

		response.write(JSON.stringify(users))
		response.end()
	}

	{
	if(request.url == '/users' && request.method == 'POST'){
		// 1. Declare a placeholder variable in order to be re-assigned to later on. This variable will be assigned the data inside the body from Postman
		let request_body = ''

		// 2. When data is detected, run a function that assigns that data to  the empty request_body variable
		request.on('data', function(data){ //request.on can have either of these three values: data, error, end
			request_body += data
		})
		// 3. Before the request ends, we want to add the new data to our existing users array of objects
		request.on('end', function(){
			//console.log(typeof request_body)
			// 4. Convert the request_body from JSON to a JS Obbject, then assign that converted value back to the request_body variable.
			request_body = JSON.parse(request_body)
			// 5. Declare a new_user variable signifying the new data that came from the Postman body.
			let new_user = {
				"name": request_body.name,
				"email": request_body.email
			}
			// 6. Add the new_user variable (object) into the users array
			users.push(new_user)
			console.log(users)
			// 7.  Write the header for the response. Make sure the content type is 'application/json' since we are writing/returning a JSON
			response.writeHead(200, {'Content-Type': 'application/json'})

			response.write(JSON.stringify(new_user))
			response.end()
		})
		
	}

	
}

})

server.listen(port)
console.log(`Server is running on localhost:${port}`)